(function(angular) {

    class controller {
        constructor(HandleDataService) {
            this.HandleDataService = HandleDataService;
            this.data = [];
            this.title = '';
        }

        getTitle(title) {
            this.title = ''
            return this.HandleDataService.addTodo(title)
        }
    }

    return angular
        .module('ToDoApp')
        .component('inputTodo', {
            templateUrl: 'input/input-todo.template.html',
            controller
        });

})(angular);


