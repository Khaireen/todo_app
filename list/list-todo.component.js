(function (angular) {

    class controller {
        constructor(HandleDataService) {
            this.HandleDataService = HandleDataService;
        }

        showTodo() {
            return this.HandleDataService.showTodo();
        }

        removeTodo(id) {
            return this.HandleDataService.removeTodo(id);
        }

        complete(id) {
            return this.HandleDataService.markCompleted(id);
        }
    }
   
    return angular
        .module('ToDoApp')
        .component('listTodo', {
            templateUrl: 'list/list-todo.template.html',
            controller
        });
})(angular)