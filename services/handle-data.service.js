(function (angular) {

    class HandleDataService {
        constructor() {

            this.data = [{
                id: 1,
                title: 'Example',
                completed: false
            }]
        }

        addTodo(title) {
            if (title != '' && title != undefined) {
                console.log(title);
                this.data.push({ id: this.data.length + 1, title: title, completed: false });
            }
            else {
                alert('Please write your TODO');
            }

        }

        showTodo() {
            return this.data;
        }

        removeTodo(index) {
            this.data.splice(index, 1);
        }

        markCompleted(index) {
            let isCompleted = this.data[index].completed;

            if (isCompleted === false) {
                this.data[index].completed = true;
            }
            else {
                this.data[index].completed = false;
            }
        }
    }

    return angular
        .module('ToDoApp')
        .service('HandleDataService', HandleDataService);

})(angular);